package com.assignments.converter;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    Button btn, btn2;
    EditText text, text2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        text = (EditText) findViewById(R.id.editTextMl);
        text2 = (EditText) findViewById(R.id.editTextInches);
        text2.setEnabled(false);


        btn = (Button) findViewById(R.id.btnConvert);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (text.getText().toString().length() == 0 && text2.getText().toString().length() == 0) {
                    Toast.makeText(MainActivity.this, "Please enter a value", Toast.LENGTH_SHORT).show();

                } else {
                    calculateInches();

                }

            }
        });

        btn2 = (Button) findViewById(R.id.btnExit);
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void calculateInches() {
        double mm = Double.parseDouble(text.getText().toString());
        double inches = mm / 25.4;


        text2.setText(String.format("%s", inches));


    }
}
